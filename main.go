package main

import (
	"image"
	"io"
	"log"
	"os"
	"time"

	"github.com/hajimehoshi/ebiten"
)

func main() {
	f, err := os.Open("roms/tetris.rom")
	rom, err := LoadRom(f)
	if err != nil {
		panic(err)
	}

	OpenGLExample(rom)
}

func OpenGLExample(rom []byte) {
	cpu = NewChip8(rom)
	screenPixels = image.NewRGBA(image.Rect(0, 0, screenWidth, screenHeight))
	tickend := time.Now().UnixNano()
	fpsend := time.Now().UnixNano()
	//600hz
	target := int64((10000000 * 100) / 600)
	//60hz
	targetFps := int64((10000000 * 100) / 60)
	InitAudio()
	go func() {
		for {
			framestart := time.Now().UnixNano()
			tickStart := framestart - tickend
			fpsStart := framestart - fpsend

			if tickStart > target {
				cpu.execute(cpu.fetchOp())
				if fpsStart > targetFps {
					cpu.tick()
					fpsend = time.Now().UnixNano()
				}
				tickend = time.Now().UnixNano()
			}
		}
	}()

	if err := ebiten.Run(OpenGLUpdate, screenWidth, screenHeight, 20, "Chip-8 (Tetris)"); err != nil {
		log.Fatal(err)
	}

}

//LoadRom from file.
func LoadRom(f *os.File) ([]byte, error) {
	defer f.Close()
	out := make([]byte, 0)
	buf := make([]byte, 1)
	defer f.Close()
	for {
		_, err := f.Read(buf)
		if err != nil {
			if err != io.EOF {
				return nil, err
			}
			break
		}
		out = append(out, buf...)
	}
	return out, nil
}
