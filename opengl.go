package main

import (
	"image"
	"log"
	"os"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/audio"
	"github.com/hajimehoshi/ebiten/audio/wav"
)

const (
	screenWidth  = 64
	screenHeight = 32
	sampleRate   = 44100
)

var (
	screenPixels *image.RGBA
	cpu          *chip8
	audioContext *audio.Context
	audioPlayer  *audio.Player
)

func SetPixel(r, g, b, a byte, h, w int) {
	screenPixels.Pix[4*(screenWidth*h+w)] = r
	screenPixels.Pix[4*(screenWidth*h+w)+1] = g
	screenPixels.Pix[4*(screenWidth*h+w)+2] = b
	screenPixels.Pix[4*(screenWidth*h+w)+3] = a
}

func InitAudio() {
	audioContext, err := audio.NewContext(sampleRate)
	if err != nil {
		log.Fatal(err)
	}
	f, err := os.Open("beep.wav")
	if err != nil {
		panic(err)
	}
	d, err := wav.Decode(audioContext, f)
	if err != nil {
		log.Fatal(err)
	}
	audioPlayer, err = audio.NewPlayer(audioContext, d)
	if err != nil {
		log.Fatal(err)
	}
}

func OpenGLUpdate(screen *ebiten.Image) error {

	_createKeyPad()

	if !audioPlayer.IsPlaying() {
		if ebiten.IsKeyPressed(ebiten.KeyP) || cpu.sound != 0 {
			audioPlayer.Rewind()
			audioPlayer.Play()
		}
	}

	if cpu.draw {
		m := 0
		for j := 0; j < screenHeight; j++ {
			for i := 0; i < screenWidth; i++ {
				if cpu.graphics[m] == 1 {
					SetPixel(0xFF, 0xFF, 0xFF, 0xFF, j, i)
				} else {
					SetPixel(0x00, 0x00, 0x00, 0x00, j, i)
				}
				m++
			}
		}
		cpu.draw = false
	}
	if ebiten.IsDrawingSkipped() {
		return nil
	}
	screen.ReplacePixels(screenPixels.Pix)

	return nil
}

func _assignKey(k ebiten.Key, index int) {
	if ebiten.IsKeyPressed(k) {
		cpu.key[index] = byte(1)
	} else {
		cpu.key[index] = byte(0)
	}
}

func _createKeyPad() {
	_assignKey(ebiten.Key1, 0)
	_assignKey(ebiten.Key2, 1)
	_assignKey(ebiten.Key3, 2)
	_assignKey(ebiten.Key4, 3)
	_assignKey(ebiten.KeyQ, 4)
	_assignKey(ebiten.KeyW, 5)
	_assignKey(ebiten.KeyE, 6)
	_assignKey(ebiten.KeyR, 7)
	_assignKey(ebiten.KeyA, 8)
	_assignKey(ebiten.KeyS, 9)
	_assignKey(ebiten.KeyD, 10)
	_assignKey(ebiten.KeyF, 11)
	_assignKey(ebiten.KeyZ, 12)
	_assignKey(ebiten.KeyX, 13)
	_assignKey(ebiten.KeyC, 14)
	_assignKey(ebiten.KeyV, 15)
}
