package main

import (
	"fmt"
	"time"
)

func debug() {

	frameend := int64(0)

	tickend := time.Now().UnixNano()
	fpsend := time.Now().UnixNano()

	//target = 10000000*100
	//100 milliseconds = 1 second
	//500hz
	//500cycles/second
	target := int64((10000000 * 100) / 500)
	targetFps := int64((10000000 * 100) / 60)

	for {

		//start of frame
		framestart := time.Now().UnixNano()
		tickStart := framestart - tickend
		fpsStart := framestart - fpsend

		//target 500hz
		if tickStart > target {
			fmt.Println("tick 500hz: ", tickStart-target, frameend)
			if fpsStart > targetFps {
				fmt.Println("tick video, audio", fpsend)
				fpsend = time.Now().UnixNano()
			}
			tickend = time.Now().UnixNano()
		}

		frameend = time.Now().UnixNano() - framestart
	}

}
