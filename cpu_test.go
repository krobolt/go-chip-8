package main

import (
	"testing"
)

func Test_getOpCode(t *testing.T) {
	memory := make(map[uint16]uint16)
	memory[0] = uint16(0xA2)
	memory[1] = uint16(0xF0)
	expected := uint16(0xA2F0)
	actual := _getOpCode(memory[0], memory[1])
	if actual != expected {
		t.Error("not same")
	}
}

func Test_executeCode(t *testing.T) {
	op := uint16(0xA2F0)
	expected := uint16(0x02F0)
	actual := _extractCode(op)
	if actual != expected {
		t.Error("not same")
	}
}
