package main

import (
	"log"
)

type opChip8 struct {
	x   uint16
	y   uint16
	kk  uint16
	nnn uint16
}

func (c *chip8) extractOp(op uint16) *opChip8 {
	return &opChip8{
		x:   GetAddressX(op),
		y:   GetAddressY(op),
		kk:  GetAddressKK(op),
		nnn: GetAddressNNN(op),
	}
}

func GetAddressX(op uint16) uint16 {
	return (op & 0x0F00) >> 8
}

func GetAddressY(op uint16) uint16 {
	return (op & 0x00F0) >> 4
}

func GetAddressN(op uint16) uint16 {
	return op & 0x000F
}

func GetAddressKK(op uint16) uint16 {
	return op & 0x00FF
}

func GetAddressNNN(op uint16) uint16 {
	return op & 0x0FFF
}

//Ins_ClearScreen instruction clear screen
func (c *chip8) InsClearScreen(op uint16) {
	log.Printf("0x%x: Clears the screen. \n", op)
	for i := range c.graphics {
		c.graphics[i] = 0x00
	}
	c.draw = true
	c.pc += 2
}

func (c *chip8) InsCallSub(op uint16) {
	//*(0xNNN)() Calls subroutine at NNN.
	log.Printf("0x%x: [0xNNN] Calls subroutine at 0x%x. \n", op, int(op&0x0FFF))
	c.stack[c.sp] = c.pc     // Store current address in stack
	c.sp++                   // Increment stack pointer
	c.pc = GetAddressNNN(op) // Set the program counter to the address at NNN
}

//Ins_RtnSub instruction return from subroutine
func (c *chip8) InsRtnSub(op uint16) {
	log.Printf("0x%x: Return from subroutine", op)
	c.sp--
	c.pc = c.stack[c.sp]
	c.pc += 2
}

//Ins_1NNN Flow goto NNN; Jumps to address NNN.
func (c *chip8) InsGotoNNN(op uint16) {
	log.Printf("0x%x: 1NNN Flow Jumps to address NNN", op)
	c.pc = GetAddressNNN(op)
}

//Ins_SkipVxNNN skip next instruction if Vx = NNN.
func (c *chip8) InsSkipVxKK(op uint16) {
	x := GetAddressX(op)
	kk := GetAddressKK(op)
	c.pc += 2
	if c.v[x] == byte(kk) {
		c.pc += 2
	}
	log.Printf("0x%x Skips the next instruction if V[0x%x] equals KK [0x%x].\n", op, x, kk)
}

//InsSkipVxNotKK 4XNN Skips the next instruction if VX doesn't equal NN
func (c *chip8) InsSkipVxNotKK(op uint16) {
	x := GetAddressX(op)
	n := GetAddressNNN(op)
	c.pc += 2
	if c.v[x] != byte(n) {
		c.pc += 2
	}
	log.Printf("0x%x Skips the next instruction if V[0x%x] != KK [0x%x].\n", op, x, n)
}

//InsSkipVxVy 5xy0 Skip next instruction if Vx = Vy.
func (c *chip8) InsSkipVxVy(op uint16) {
	x := GetAddressX(op)
	y := GetAddressY(op)
	if c.v[x] == c.v[y] {
		c.pc += 2
	}
	c.pc += 2
	log.Printf("0x%x Skips the next instruction if V[0x%x] == V[0x%x].\n", op, x, y)
}

//InsSetVXKK 6XKK Const Sets VX to KK.
func (c *chip8) InsSetVXKK(op uint16) {
	x := GetAddressX(op)
	kk := GetAddressKK(op)
	c.v[x] = byte(kk)
	c.pc += 2
	log.Printf("0x%x 6XKK Const Sets V[0x%x] = 0x%x .\n", op, x, kk)
}

//InsAddKKtoVX 7XKK Adds NN to VX. (Carry flag is not changed)
func (c *chip8) InsAddKKtoVX(op uint16) {
	//Vx += NN (Carry flag is not changed)
	x := GetAddressX(op)
	kk := GetAddressKK(op)
	c.v[x] = c.v[x] + byte(kk)
	c.pc += 2
}

//InsLDVxVy Stores the value of register Vy in register Vx.
func (c *chip8) InsLDVxVy(x, y uint16) {
	// 8xy0 - LD Vx, Vy
	// Set Vx = Vy.
	// Stores the value of register Vy in register Vx.
	c.v[x] = c.v[y]
	c.pc += 2
}

// Performs a bitwise OR on the values of Vx and Vy,
// then stores the result in Vx
func (c *chip8) InsLDORVxVy(x, y uint16) {
	// Set Vx = Vx OR Vy.
	c.v[x] = c.v[x] | c.v[y]
	c.pc += 2
}

// Set Vx = Vx AND Vy.
func (c *chip8) InsLDANDVxVy(x, y uint16) {
	// Set Vx = Vx AND Vy.
	c.v[x] = c.v[x] & c.v[y]
	c.pc += 2
}

// Set Vx = Vx XOR Vy.
func (c *chip8) InsLDXORVxVy(x, y uint16) {

	// Performs a bitwise exclusive OR on the values of Vx
	// and Vy, then stores the result in Vx. An exclusive OR
	// compares the corrseponding bits from two values, and
	// if the bits are not both the same, then the
	// corresponding bit in the result is set to 1.
	// Otherwise, it is 0.
	c.v[x] = c.v[x] ^ c.v[y]
	c.pc += 2
}

//InsADDVxVy 8xy4 - ADD Vx, Vy
func (c *chip8) InsADDVxVy(x, y uint16) {
	// Set Vx = Vx + Vy, set VF = carry.
	// The values of Vx and Vy are added together. If the
	// result is greater than 8 bits (i.e., > 255,) VF is
	// set to 1, otherwise 0. Only the lowest 8 bits of the
	// result are kept, and stored in Vx.
	r := uint16(c.v[x]) + uint16(c.v[y])
	var cf byte
	if r > 0xFF {
		cf = 1
	}
	c.v[0xF] = cf
	c.v[x] = byte(r)
	c.pc += 2
}

//InsSUBVxVy 8xy5 - SUB Vx, Vy
func (c *chip8) InsSUBVxVy(x, y uint16) {
	// Set Vx = Vx - Vy, set VF = NOT borrow.
	// If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy
	// is subtracted from Vx, and the results stored in Vx.
	if c.v[x] > c.v[y] {
		c.v[0xF] = 1
	} else {
		c.v[0xF] = 0
	}
	c.v[x] = c.v[y] - c.v[x]
	c.pc += 2
}

// InsSHRVx 8xy7 - SUBN Vx, Vy
// Set Vx = Vx SHR 1.
func (c *chip8) InsSHRVx(x uint16) {

	//8XY6 	BitOp
	//Vx>>=1 	Stores the least significant bit of VX in VF
	//and then shifts VX to the right by 1.

	//If the least-significant bit of Vx is 1,
	//						then VF is set to 1, otherwise 0.
	// Then Vx is divided by 2.

	// If the least-significant bit of Vx is 1, then VF is
	// set to 1, otherwise 0. Then Vx is divided by 2.
	if (c.v[x] & 0x01) == 0x01 {
		c.v[0xF] = 1
	} else {
		c.v[0xF] = 0
	}
	c.v[x] = c.v[x] >> 1
	c.pc += 2
}

//InsSUBN 8xy7 - SUBN Vx, Vy
func (c *chip8) InsSUBN(x, y uint16) {
	// Set Vx = Vy - Vx, set VF = NOT borrow.
	// If Vy > Vx, then VF is set to 1, otherwise 0.
	//Then Vx is subtracted from Vy, and the results stored in Vx.
	if c.v[y] > c.v[x] {
		c.v[0xF] = 1
	} else {
		c.v[0xF] = 0
	}
	c.v[x] = c.v[y] - c.v[x]
	c.pc += 2
}

func (c *chip8) InsSHLVx(x uint16) {
	// If the most-significant bit of Vx is 1, then VF is
	// set to 1, otherwise to 0. Then Vx is multiplied by 2.
	if (c.v[x] & 0x80) == 0x80 {
		c.v[0xF] = 1
	} else {
		c.v[0xF] = 0
	}
	c.v[x] = c.v[x] * 2
	c.pc += 2
}
